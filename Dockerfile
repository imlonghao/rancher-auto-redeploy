FROM python:3.7-alpine
LABEL maintainer="imlonghao <dockerfile@esd.cc>"
WORKDIR /app
COPY app.py ./
COPY rancher.py ./
RUN apk add --no-cache --virtual .build-deps g++ make && \
    pip install --no-cache-dir pyyaml sanic aiohttp && \
    apk del .build-deps
EXPOSE 20000
ENTRYPOINT [ "/app/app.py" ]