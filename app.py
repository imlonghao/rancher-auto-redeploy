#!/usr/bin/env python3

import yaml
import asyncio
from os import environ
from sanic import Sanic, response
from rancher import Rancher

r = Rancher(
    environ.get('R-BASEURL'),
    environ.get('R-ACCESS-KEY'),
    environ.get('R-SECRET-KEY'))
app = Sanic(__name__)


async def service_upgrade_worker(service_id):
    print(service_id + ': Starting re-deploy service')
    while (await r.service_status(service_id)) != 'active':
        print(service_id + ': Other job is running')
        await asyncio.sleep(1)
    if (await r.service_upgrade(service_id)) == False:
        print(service_id + ': Error in service upgrade')
        return
    while (await r.service_status(service_id)) != 'upgraded':
        print(service_id + ': Upgrade in process')
        await asyncio.sleep(1)
    if (await r.service_finish_upgrade(service_id)) == False:
        print(service_id + ': Error in service finish upgrade')
        return
    print(service_id + ': Re-deploy finished')


def validator_gitlab(conf, requests):
    if requests.headers.get('X-Gitlab-Event') != 'Pipeline Hook':
        return False
    if requests.headers.get('X-Gitlab-Token') != conf.get('token'):
        return False
    if requests.json.get('object_attributes').get('status') != 'success':
        return False
    return True


def validator_dockerhub(conf, requests):
    token = requests.args.get('token')
    if token is None or token != conf.get('token'):
        return False
    return True


def validator(func):
    def wrapper(requests, service_id):
        with open('./data/config.yml', 'r') as f:
            conf = yaml.load(f).get(service_id, False)
        if not conf:
            return response.json({'status': 403}, 403)
        ct = conf.get('type')
        if ct == 'gitlab':
            if validator_gitlab(conf, requests):
                return func(requests, service_id)
        elif ct == 'dockerhub':
            if validator_dockerhub(conf, requests):
                return func(requests, service_id)
        return response.json({'status': 403}, 403)
    return wrapper


@app.post(r'/<service_id:\d+s\d+>')
@validator
async def webhook(requests, service_id):
    asyncio.ensure_future(service_upgrade_worker(service_id), loop=app.loop)
    return response.json({'status': 202}, 202)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=20000)
