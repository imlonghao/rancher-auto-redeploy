#!/usr/bin/env python3

import aiohttp


class Rancher():
    def __init__(self, url, access_key, secret_key):
        self.base_url = url
        self.auth = aiohttp.BasicAuth(access_key, secret_key)

    async def __get(self, url):
        async with aiohttp.ClientSession(auth=self.auth) as session:
            async with session.get(self.base_url + url) as result:
                return [result.status, await result.json()]

    async def __post(self, url, data):
        async with aiohttp.ClientSession(auth=self.auth) as session:
            async with session.post(self.base_url + url, json=data) as result:
                return [result.status, await result.json()]

    async def service_status(self, service_id):
        result = await self.__get(service_id)
        return result[1].get('state')

    async def service_upgrade(self, service_id):
        info = await self.__get(service_id)
        data = {
            'inServiceStrategy': {
                'batchSize': 1,
                'intervalMillis': 2000,
                'launchConfig': info[1].get('launchConfig'),
                'secondaryLaunchConfigs': info[1].get('secondaryLaunchConfigs'),
                'startFirst': False
            }
        }
        result = await self.__post(service_id + '/?action=upgrade', data=data)
        return result[0] == 202

    async def service_finish_upgrade(self, service_id):
        result = await self.__post(service_id + '/?action=finishupgrade', data={})
        return result[0] == 202
